package com.enkeapple.user.controller;

import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.dto.UserDTO;
import com.enkeapple.user.mapper.BasePageableMapper;
import com.enkeapple.user.mapper.UserMapper;
import com.enkeapple.user.mapper.UserPageableMapper;
import com.enkeapple.user.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "Users")
@Validated
@RequestMapping("/api/v1/user")
@FieldDefaults(level = AccessLevel.PROTECTED)
public class UserController {
    final UserService userService;
    final UserMapper userMapper;
    final UserPageableMapper userPageableMapper;

    @Autowired
    public UserController(UserService userService, UserMapper userMapper, UserPageableMapper userPageableMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.userPageableMapper = userPageableMapper;
    }

    @GetMapping()
    @Cacheable("users")
    public ResponseEntity<BasePageableMapper<UserDTO>> getAll(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "5") Integer size
    ) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id").ascending());

        Page<UserDTO> result = userService.findAll(pageable).map(userMapper::entityToDto);

        BasePageableMapper<UserDTO> response = userPageableMapper.convertPageToMap(result);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserDTO> save(@Valid @RequestBody UserDTO body) {
        UserEntity entity = userService.create(userMapper.dtoToEntity(body));

        return ResponseEntity.status(HttpStatus.CREATED).body(userMapper.entityToDto(entity));
    }

    @GetMapping("/{id}")
    @Cacheable("users")
    public ResponseEntity<UserDTO> findById(@PathVariable long id) {
        UserEntity entity = userService.findById(id);

        if (entity != null) {
            return ResponseEntity.ok(userMapper.entityToDto(entity));
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @CacheEvict("users")
    public ResponseEntity<String> delete(@PathVariable long id) {
        boolean deleted = userService.delete(id);

        if (deleted) {
            return new ResponseEntity<>("User with ID " + id + " has been successfully deleted.", HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND.getReasonPhrase(), HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    @CachePut("users")
    public ResponseEntity<String> update(@PathVariable long id, @Valid @RequestBody UserDTO body) {
        UserEntity entity = userMapper.dtoToEntity(body);

        entity.setId(id);

        userService.update(entity);

        return new ResponseEntity<>("User with ID " + id + " has been successfully updated.", HttpStatus.OK);
    }
}
