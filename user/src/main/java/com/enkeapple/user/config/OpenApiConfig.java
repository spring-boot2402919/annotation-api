package com.enkeapple.user.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI generateOpenAPI() {
        OpenAPI openAPI = new OpenAPI();

        Info info = new Info();

        info.title("Annotation users API documentation")
                .version("1.0.0")
                .description("API documentation for users api project");

        return openAPI.info(info);
    }
}
