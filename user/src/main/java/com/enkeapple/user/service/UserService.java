package com.enkeapple.user.service;

import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PROTECTED)
public class UserService implements IService<UserEntity> {
    final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserEntity create(UserEntity entity) {
        return userRepository.save(entity);
    }

    @Override
    @Transactional
    public UserEntity update(UserEntity entity) {
        return userRepository.saveAndFlush(entity);
    }

    @Override
    public Page<UserEntity> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    @Transactional
    public UserEntity findById(Long id) {
        Specification<UserEntity> specification = (root, query, cb) -> cb.equal(root.get("id"), id);

        return userRepository.findOne(specification)
                .orElseThrow(() -> new EntityNotFoundException("User with ID " + id + " was not found in the system."));
    }

    @Override
    public boolean delete(Long id) {
        Specification<UserEntity> specification = (root, query, cb) -> cb.equal(root.get("id"), id);

        userRepository.findOne(specification)
                .orElseThrow(() -> new EntityNotFoundException("User with ID " + id + " was not found in the system."));

        userRepository.deleteById(id);

        return true;
    }
}
