package com.enkeapple.user.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface IService<E> {
    E create(E entity);

    E update(E entity);

    Page<E> findAll(Pageable pageable);

    boolean delete(Long id);

    E findById(Long id);
}
