package com.enkeapple.user.exception;

import com.enkeapple.user.mapper.UserResponseMapper;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(UserException.class)
    public ResponseEntity<UserResponseMapper> handleUserException(UserException exc) {
        UserResponseMapper response = UserResponseMapper.builder()
                .message(exc.getMessage())
                .status(HttpStatus.BAD_REQUEST.value())
                .timestamp(System.currentTimeMillis())
                .build();

        return new ResponseEntity<>(response, exc.getStatus());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<UserResponseMapper> handleArgumentNotValidException(EntityNotFoundException exc) {
        UserResponseMapper response = UserResponseMapper.builder()
                .status(HttpStatus.NOT_FOUND.value())
                .message(exc.getMessage())
                .timestamp(System.currentTimeMillis())
                .build();

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<UserResponseMapper> handleArgumentNotValidException(MethodArgumentNotValidException exc) {
        UserResponseMapper response = UserResponseMapper.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message(String.valueOf(exc.getFieldError().getDefaultMessage()))
                .timestamp(System.currentTimeMillis())
                .build();

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
