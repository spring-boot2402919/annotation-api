package com.enkeapple.user.mapper;

import org.mapstruct.Mapping;

public interface BaseMapper<E, D> {
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    E dtoToEntity(D dto);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    D entityToDto(E entity);
}
