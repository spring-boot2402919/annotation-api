package com.enkeapple.user.mapper;

import com.enkeapple.user.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class UserPageableMapper implements IBasePageableMapper<UserDTO> {
    @Override
    public BasePageableMapper<UserDTO> convertPageToMap(Page<UserDTO> page) {
        return BasePageableMapper.<UserDTO>builder()
                .total(page.getTotalElements())
                .page(page.getNumber())
                .size(page.getSize())
                .data(page.getContent())
                .build();
    }
}
