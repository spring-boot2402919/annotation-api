package com.enkeapple.user.mapper;

import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {UserMapper.class}
)
public interface UserMapper extends BaseMapper<UserEntity, UserDTO> {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
}