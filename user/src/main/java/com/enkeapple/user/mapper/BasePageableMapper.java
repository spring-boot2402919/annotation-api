package com.enkeapple.user.mapper;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BasePageableMapper<D> {
    long total;
    int size;
    int page;
    List<D> data;
}
