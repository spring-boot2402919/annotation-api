package com.enkeapple.user.mapper;

import org.springframework.data.domain.Page;

public interface IBasePageableMapper<D> {
    BasePageableMapper<D> convertPageToMap(Page<D> page);
}
