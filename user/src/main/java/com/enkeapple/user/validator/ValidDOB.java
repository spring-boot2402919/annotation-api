package com.enkeapple.user.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DOBValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidDOB {
    String message() default "Age must be between 16 and 96 years";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}