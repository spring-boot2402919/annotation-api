package com.enkeapple.user.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;
import java.time.Period;

public class DOBValidator implements ConstraintValidator<ValidDOB, LocalDate> {
    public void initialize(ValidDOB annotation) {}


    @Override
    public boolean isValid(LocalDate dob, ConstraintValidatorContext context) {
        if (dob == null) {
            return false;
        }

        int age = Period.between(dob, LocalDate.now()).getYears();

        return age >= 16 && age <= 96;
    }
}